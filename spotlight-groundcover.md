# OpenMW Spotlight: Cover the ground!

Hello again,

Since OpenMW implemented object paging, we have been enjoying long viewing distances with distant objects with playable performances. Some have even been able to run grass mods without much issue. With emphasis on the word "some". Object paging is not mainly implemented with groundcover mods in mind, even though it might work out okay for that too.

Enter instancing. Our veteran akortunov has, together with Master of Shadows AnyOldName3, been working on supporting groundcover through a system much more suitable for it. You see, supporting grass and other groundcover require some special tweaking before you can get acceptable performance with it. Otherwise, each an every piece of grass will have to be sent to the CPU before the GPU can render it, and this takes a very long time. This is called a "draw call". Draw calls are slow. So slow that you don't want too many of them in one scene. Reducing draw calls was actually the reason we implemented Active Grid Object Paging.

So while Object Paging worked out great for buildings and similar, grass can get handled better, especially since you want grass to sway with the wind and to get bent when you step on it. The new, already implemented, groundcover system enables grass and other groundcover with better performance and with full support for animation and reaction to you stepping on it.

So far, work has been focused on supporting legacy mods previously made for MGE XE. This puts some limitations on what can be done. In the future, we plan to implement an even better system to handle groundcover. Such a system will, however, not be compatible with current mods made for MGE XE.

Try the groundcover system today in a nightly version, or wait for the next release. You can read on how to enable groundcover mods [here](https://openmw.readthedocs.io/en/latest/reference/modding/settings/groundcover.html).
