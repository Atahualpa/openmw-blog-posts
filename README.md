# OpenMW blog posts

Here we can create the future blog posts for OpenMW together. I don't know if it is better to have this in the OpenMW repository instead, but whatever, I created it here on my profile now.

## How it should be done

My idea is that each blog post is written in a new markdown file (.md) and that proof-reading can be submitted either through regular reviewing or through merge requests. Merge requests are proably the prefered way. Everyone are free to make blog posts even though we have them in my repository right now. Just make a merge request for it and I'll merge it right away so we can get the proof-reading started.

If we believe we should have all this over at OpenMW's gitlab account, I'm all fine with that. We can just fork this in that case. But I'm all fine if we keep them here at my account.

## Categories for blog posts

Martin (Atahualpa) had a suggestion of categories that could serve well for inspiration:

- "The Month in Review": more casual project updates, covering more topics than other posts
- "Inside OpenMW": more technical posts about current coding challenges and progress (distant statics, shadows, ...)
- "Ask a Developer": interviews with developers (if we plan to do more of these)
- "OpenMW Release Affairs": everything regarding the release process (entering RC phase, nearing release)
- "OpenMW x.y.z Released!": regular OpenMW release announcement

Other ideas for posts are of course okay as well, but it is probably a good thing to have some kind of reoccuring "theme" like this for the posts.